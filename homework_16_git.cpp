﻿#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <ctime>
#include <stdio.h>
#include <time.h>
using namespace std;

#define N 3

void printArray(int array[N][N])
{
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            cout << array[i][j];
        }
        cout << endl;
    }
}

void sumElements(int array[N][N])
{
    int sum = 0;
    
    time_t t = time(0);
    tm* now = localtime(&t);
    int k = ((now->tm_mday) % N);

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            if (i == k)
            {
                sum += array[i][j];
            }
        }
    }

    cout << "Sum = " << sum << endl;
}

int main()
{
    int array[N][N];

    cout << "Array:\n";
    
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            array[i][j] = i + j;
        }
    }

    printArray(array);
    sumElements(array);

    return 0;
}